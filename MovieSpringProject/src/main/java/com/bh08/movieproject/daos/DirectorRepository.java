package com.bh08.movieproject.daos;

import com.bh08.movieproject.models.Director;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Long> {

    List<Director> findByName(String name);
}
