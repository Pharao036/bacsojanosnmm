package com.bh08.movieproject.daos;

import com.bh08.movieproject.models.Category;
import com.bh08.movieproject.models.MovieCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieCategoryRepository extends JpaRepository<MovieCategory, Long> {
    List<MovieCategory> findByCategory(Category category);
}
