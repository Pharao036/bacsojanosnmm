package com.bh08.movieproject.daos;

import com.bh08.movieproject.models.Chair;
import com.bh08.movieproject.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChairRepository extends JpaRepository<Chair, Long> {

    public Chair findByRowOfChairAndColumnOfChairAndRoom(char rowOfChair, int columnOfChair, Room room);

    public Chair findFirstById(Long ChairId);

}
