package com.bh08.movieproject.daos;

import com.bh08.movieproject.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>  {
    
}
